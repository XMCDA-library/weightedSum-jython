JYTHON=
XMCDA_LIB="/path/to/XMCDA-java.jar"
# Required: Java 8
JAVA_HOME=

# -- You normally do not need to change anything beyond this point --

if [ "" != "${JAVA_HOME}" ]; then
  export JAVA_HOME
fi
export JYTHONPATH="${XMCDA_LIB}"

# check that the XMCDA library can be found and that Java is executable
if [ ! -f ${XMCDA_LIB} ]; then
  echo "common_settings.sh: XMCDA_LIB not found. Please modify common_settings.sh to reflect your installation (see README)" >&2
  exit -1;
fi
if [ ! -x ${JYTHON} ]; then
  echo "common_settings.sh: Jython not found. Please modify common_settings.sh to reflect your installation (see README)" >&2
  exit -1;
fi
if [ ! -x "${JAVA_HOME}/bin/java" ]; then
  echo "JAVA: ${JAVA_HOME}/bin/java: not found -- please edit common_settings.sh and check JAVA_HOME"
  exit -1;
fi

CMD="${JYTHON} src/weightedSumCLI.py"
