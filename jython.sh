#! /bin/bash
JAVA_HOME=/usr/local/java8
JYTHON=/home/big/lang/python/jython2.7.0/bin/jython
XMCDA_LIB="/home/TB/projects/mcda/xmcda/XMCDA-java/XMCDA-java.jar"

#export CLASSPATH="${XMCDA_LIB}:${CLASSPATH}"
export JAVA_HOME
export JYTHONPATH="${XMCDA_LIB}"

#"${JAVA}" -cp "${JYTHON_STANDALONE_JAR}:${CLASSPATH}" org.python.util.jython
"${JYTHON}" "$@"

exit $?
