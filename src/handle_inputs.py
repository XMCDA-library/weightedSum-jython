from java.lang import Throwable

import utils


WEIGHTED_SUM = "weightedSum"
NORMALIZED_WEIGHTED_SUM = "normalizedWeightedSum"
SUM = "sum"
AVERAGE = "average"
AGGREGATION_OPERATORS = (WEIGHTED_SUM, NORMALIZED_WEIGHTED_SUM, SUM, AVERAGE)

class Inputs:
    """
    Class Inputs gathers all the information retrieved from the XMCDA inputs
    """
    def __init__(self):
        # tells if criteriaWeights.xml is present and contains <criteriaValues>. It may be empty.
        self.has_weights = False
        # Indicates the aggregation operator present in parameters.xml
        self.operator = None
        # contains the performance table supplied in performanceTable.xml
        self.performance_table = {}
        # contains the list of alternatives_ids
        self.alternatives_ids = []
        # contains the <criterionValue>s
        self.weights = []


def check_and_extract_inputs(xmcda, xmcda_exec_results):
    """

    :param xmcda:
    :param xmcda_exec_results:
    :return:
    """
    inputs = check_inputs(xmcda, xmcda_exec_results)

    if not (xmcda_exec_results.isOk() or xmcda_exec_results.isWarning()):
        return None

    return extract_inputs(inputs, xmcda, xmcda_exec_results)


def check_inputs(xmcda, xmcda_exec_results):
    """

    :param xmcda:
    :param xmcda_exec_results:
    :return:
    """
    inputs = Inputs()
    # Get & check the performance table object
    if len(xmcda.performanceTablesList) == 0:
        xmcda_exec_results.addError("No performance table has been supplied")
    elif len(xmcda.performanceTablesList) > 1:
        xmcda_exec_results.addError("More than one performance table has been supplied")
    else:
        p = xmcda.performanceTablesList.get(0)

        if p.hasMissingValues():
            xmcda_exec_results.addError("The performance table has missing values")
        # Anyhow, it must be numeric here
        if not p.isNumeric():
            xmcda_exec_results.addError("The performance table must contain numeric values only")
        else:
            # convert all values as Double
            try:
                perf_table = p.asDouble()
                xmcda.performanceTablesList.set(0, perf_table)
            except Throwable as t:
                # Should not happen because we already checked that 'p.isNumeric()' is true
                # However, bugs are always possible... so let's not ignore the throwable
                xmcda_exec_results.addError(utils.get_message("Error when converting the performance table's value to Double, reason:", t))

    # Let's check the parameters
    while True:
        if len(xmcda.programParametersList) > 1:
            xmcda_exec_results.addError("Only one programParameter is expected")
            break

        if len(xmcda.programParametersList) == 0:
            xmcda_exec_results.addError("No programParameter found")
            break

        if len(xmcda.programParametersList.get(0)) != 1:
            xmcda_exec_results.addError("Parameters' list must contain exactly one element");
            break

        prg_param = xmcda.programParametersList.get(0).get(0)

        if not "operator" == prg_param.id():
            xmcda_exec_results.addError("Invalid parameter w/ id '%s'" % prg_param.id())
            break

        if prg_param.getValues() is None or (prg_param.getValues() is not None and len(prg_param.getValues()) != 1):
            xmcda_exec_results.addError("Parameter operator must have a single (label) value only")
            break

        operator = prg_param.getValues().get(0).getValue()
        if operator not in AGGREGATION_OPERATORS:
            xmcda_exec_results.addError('Invalid value for parameter operator, it must be a label, ' +
                                        'possible values are: ' + ','.join(AGGREGATION_OPERATORS))
            operator = None

        inputs.operator = operator
        break # end check parameters

    # Let's check the weights
    while True:
        inputs.has_weights = xmcda.criteriaValuesList.size() >= 1

        if xmcda.criteriaValuesList.size() > 1:
            xmcda_exec_results.addError('More than one criteriaValues supplied')
            break

        # Weights must be numeric values, independently of the parameter operator's value
        if inputs.has_weights:
            weights = xmcda.criteriaValuesList.get(0)
            if not weights.isNumeric():
                xmcda_exec_results.addError('The weights must be numeric values only')
                break

        if inputs.operator is None:
            break

        # weights should be provided for (normalized) weighted sum
        weights_are_required = inputs.operator in (WEIGHTED_SUM, NORMALIZED_WEIGHTED_SUM)

        if weights_are_required and not inputs.has_weights:
            xmcda_exec_results.addError("Criteria weights must be supplied when parameter operator is '%s'" % operator)
            break

        if not weights_are_required and inputs.has_weights:
            xmcda_exec_results.addInfo("Input weights.xml is ignored when operator is '%s'" % operator)
            break

        break  # end check weights

    return inputs


def extract_inputs(inputs, xmcda, xmcda_execution_results):
    """

    :param inputs:
    :param xmcda:
    :param xmcda_execution_results:
    :return:
    """
    performance_table = {} # alternative_id -> { criterion_id -> value }
    weights = {}  # criterion -> double

    # These two will be the ones on which the calculation will operate
    alternatives_ids = []
    criteria_ids = []

    # already converted to Double in check_inputs
    xmcda_perf_table = xmcda.performanceTablesList.get(0)

    # first, get the alternatives & criteria from the performance table
    for x_alternative in xmcda_perf_table.getAlternatives():
        if x_alternative.isActive():
            alternatives_ids.append(x_alternative.id())
    for x_criterion in xmcda_perf_table.getCriteria():
        if x_criterion.isActive():
            criteria_ids.append(x_criterion.id())

    # Check that we still have active alternatives and criteria
    if len(alternatives_ids) == 0:
        xmcda_execution_results.addError("All alternatives of the performance table are inactive")
    if len(criteria_ids) == 0:
        xmcda_execution_results.addError("All criteria of the performance table are inactive")

    # Last, check that criteria in weights has a non-null intersection with active criteria used in the performance table.
    if inputs.has_weights:
        weights_criteria_ids = []
        for x_criterion in xmcda.criteriaValuesList.get(0).getCriteria():
            weights_criteria_ids.append(x_criterion.id())
        criteria_ids = [ criterion_id for criterion_id in criteria_ids if criterion_id in weights_criteria_ids ]
        if len(criteria_ids) == 0:
            xmcda_execution_results.addError("The set of active criteria in perf.table has no common id in the set of the criteria ids used in the weights")

    # At this point there is nothing more we can check or do but abort the execution
    if xmcda_execution_results.isError():
        return None

    # build performance table
    x_perf_table = xmcda.performanceTablesList.get(0)
    for x_alternative in x_perf_table.getAlternatives():
        if x_alternative.id() not in alternatives_ids:
            continue
        performance_table[x_alternative.id()] = {}
        for x_criterion in x_perf_table.getCriteria():
            if x_criterion.id() not in criteria_ids:
                continue
            # Get the value attached to the alternative, create it if necessary
            value = x_perf_table.getValue(x_alternative, x_criterion)
            performance_table[x_alternative.id()][x_criterion.id()]=value

    inputs.performance_table = performance_table

    # build weights
    if inputs.operator in (WEIGHTED_SUM, NORMALIZED_WEIGHTED_SUM):
        for criterion in xmcda_perf_table.getCriteria():
            if criterion.id() not in criteria_ids:
                continue
            weights[criterion.id()] = float(xmcda.criteriaValuesList.get(0).get(criterion).get(0).getValue())

        if inputs.operator == NORMALIZED_WEIGHTED_SUM:
            weights_sum = 0.0
            for v in weights.values():
                weights_sum += v
            for criterion_id in weights.keys():
                weights[criterion_id] = weights[criterion_id] / weights_sum

    else:
        # either 'average' or a simple 'sum'
        for criterion_id in criteria_ids:
            if inputs.operator == AVERAGE:
                weights[criterion_id] = 1.0 / len(criteria_ids)
            else:
                weights[criterion_id] = 1.0

    inputs.alternatives_ids = alternatives_ids
    inputs.weights = weights

    return inputs
