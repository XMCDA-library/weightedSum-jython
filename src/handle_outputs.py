"""

"""
from org.xmcda import XMCDA, Alternative, AlternativesValues


XMCDA_v2_TAG_FOR_FILENAME = {
    # output name -> XMCDA v2 tag
    'alternativesValues': 'alternativesValues',
    'messages': 'methodMessages',
}

XMCDA_v3_TAG_FOR_FILENAME = {
    # output name -> XMCDA v3 tag
    'alternativesValues': 'alternativesValues',
    'messages': 'programExecutionResult',
}


def xmcda_v3_tag(outputFilename):
    return XMCDA_v3_TAG_FOR_FILENAME[outputFilename]


def xmcda_v2_tag(outputFilename):
    return XMCDA_v2_TAG_FOR_FILENAME[outputFilename]


def convert(alternatives_values, alternatives_ids, xmcda_execution_results):
    """

    :param alternatives_values:
    :param alternatives_ids:
    :param xmcda_execution_results:
    :return:
    """

    """
    Converts the outputs of the computation to XMCDA objects

    :param alternatives_values: the computed alternatives values
    :param alternatives_ids: the list of alternatives ids taken into account in the computation
    :param xmcda_execution_results: the xmcda execution results
    :return: a map with keys being the names of the outputs, and their corresponding XMCDA v3 values (NOT including 'messages.xml')
    """
    # translate the results into XMCDA v3
    xmcda_alternatives_values = XMCDA() # for XMCDA tag: alternativesValues
    x_alternatives_values = AlternativesValues()

    for alternative_id in alternatives_ids:
        x_alternatives_values.put(Alternative(alternative_id), alternatives_values[alternative_id])

    xmcda_alternatives_values.alternativesValuesList.add(x_alternatives_values)

    return {
        'alternativesValues': xmcda_alternatives_values,
        }
