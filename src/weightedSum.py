"""

"""

def calculate_weighted_sum(inputs):
    """

    :param performance_table:
    :param weights:
    :return:
    """
    performance_table = inputs.performance_table
    weights = inputs.weights
    weighted_sum = {} # criterion -> double
    for alternative_id in performance_table.keys():
        weighted_sum[alternative_id] = 0.0
        for criterion_id in performance_table[alternative_id].keys():
            weighted_sum[alternative_id] += performance_table[alternative_id][criterion_id] * weights[criterion_id]

    return weighted_sum
