from java.lang import Throwable
# import org.xmcda.utils.ValueConverters.ConversionException
from org.xmcda import XMCDA, AlternativesValues

WEIGHTED_SUM = "weightedSum"
NORMALIZED_WEIGHTED_SUM = "normalizedWeightedSum"
SUM = "sum"
AVERAGE = "average"

AGGREGATION_OPERATORS = (WEIGHTED_SUM, NORMALIZED_WEIGHTED_SUM, SUM, AVERAGE)


def calculate_weighted_sum(xmcda, operator):

    weights = {}  # criterion -> double
    # convert all values as Double
    try:
        perf_table = xmcda.performanceTablesList.get(0).asDouble()
    except Throwable as t:
        # ... even if this should not happen since this has been verified in main() / Utils.handle_inputs()
        raise ValueError("One or more performance table' values are not numerical")

    if operator not in AGGREGATION_OPERATORS:
        raise ValueError("Invalid operator %s, must be one of %s" % (operator, ','.join(AGGREGATION_OPERATORS)))

    if operator == WEIGHTED_SUM or operator == NORMALIZED_WEIGHTED_SUM:
        for criterion in perf_table.getCriteria():
            try:
                # weights[criterion] = xmcda.criteriaValuesList.get(0).get(criterion).get(0).convertTo(Double.class).getValue()
                weights[criterion] = float(xmcda.criteriaValuesList.get(0).get(criterion).get(0).getValue())
            except ValueError as e:
                raise ValueError("One or more criteria' weights are not numerical")

        if operator == NORMALIZED_WEIGHTED_SUM:
            weights_sum = 0.0
            for v in weights.values():
                weights_sum += v
            for criterion in weights.keys():
                weights[criterion] = weights[criterion] / weights_sum

    else:
        # either 'average' or a smple 'sum'
        for criterion in perf_table.getCriteria():
            if operator == AVERAGE:
                weights[criterion] = 1.0 / perf_table.getCriteria().size()
            else:
                weights[criterion] = 1.0

    # Ok, calculate the weightedSum
    alternatives_values = AlternativesValues()

    for alternative in perf_table.getAlternatives():
        if not alternative.isActive():
            continue
        for criterion in perf_table.getCriteria():
            # Get the value attached to the alternative, create it if necessary
            value_a = alternatives_values.setDefault(alternative, 0.0)

            # calculation
            value = value_a.get(0).getValue()
            value = value + (weights[criterion] * perf_table.getValue(alternative, criterion))
            value_a.get(0).setValue(value)

    # We create an other XMCDA object to store the result and write the file
    result = XMCDA()
    result.alternativesValuesList.add(alternatives_values)
    return result
