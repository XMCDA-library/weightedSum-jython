# coding=utf-8
"""
"""
import os
import sys
import getopt

import utils
import handle_inputs
import weightedSum

import org.xmcda

from java.lang import Throwable


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    input_dir = output_dir = None
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "i:o:h",
                                       ['in=', 'out=', 'help'])
            for opt, arg in opts:
                if opt in ("-h", "--help"):
                    print __doc__
                    sys.exit(0)
                elif opt in ('-i', '--in'):
                    input_dir = arg
                elif opt in ('-o', '--out'):
                    output_dir = arg
        except getopt.error, msg:
            raise Usage(msg)
    except Usage, err:
        print >> sys.stderr, err.msg
        print >> sys.stderr, "for help use --help"
        return 2

    if None in (input_dir, output_dir):
        print >> sys.stderr, "Error: both parameters input and output must be supplied"
        print >> sys.stderr, "for help use --help"
        return 2

    prg_exec_results = os.path.join(output_dir, "messages.xml")

    executionResults = org.xmcda.ProgramExecutionResult()

    # this object is where XMCDA items will be put into.
    xmcda_v3 = org.xmcda.XMCDA()
    parser = org.xmcda.parsers.xml.xmcda_v2.XMCDAParser()
    xmcda_v2 = None

    performance_table_filename = os.path.join(input_dir, "performanceTable.xml")
    if not os.path.exists(performance_table_filename):
        executionResults.addError("Could not find the mandatory file performanceTable.xml")
    else:
        try:
            xmcda_v2 = parser.readXMCDA(performance_table_filename)
        except Throwable as exc:
            executionResults.addError(utils.get_message("Unable to read & parse the performance table, reason: ", exc))

    # Read the alternatives.xml, if any, because if it is present, it may declare some alternatives to be inactive
    alternatives_file = os.path.join(input_dir, "alternatives.xml")
    if os.path.exists(alternatives_file):
        # TODO check that None are created, and that it is not a subset of performanceTable
        try:
            utils.read_xmcda_v2_file_and_update(xmcda_v2, parser, alternatives_file)
        except Throwable as t:
            executionResults.addError(utils.get_message("Unable to read & parse the supplied file for alternatives, reason: ", t))

    # Read the criteria.xml, if any
    criteria_file = os.path.join(input_dir, "criteria.xml")
    if os.path.exists(criteria_file):
        try:
            utils.read_xmcda_v2_file_and_update(xmcda_v2, parser, criteria_file)
        except Throwable as t:
            executionResults.addError(utils.get_message("Unable to read & parse the supplied file for criteria, reason: ", t))

    # do we have criteria weights? */
    criteria_weights = os.path.join(input_dir, "criteriaWeights.xml")
    if os.path.exists(criteria_weights):
        try:
            utils.read_xmcda_v2_file_and_update(xmcda_v2, parser, criteria_weights)
        except Throwable as t:
            executionResults.addError(
                utils.get_message("Unable to read & parse the supplied file for criteria weights, reason: ", t))

    # Read parameters.xml, if any
    parameters_v2 = None
    parameters_v3 = org.xmcda.XMCDA()
    parameters_file = os.path.join(input_dir, "parameters.xml")
    if os.path.exists(parameters_file):
        try:
            parameters_v2 = parser.readXMCDA(parameters_file)
        except Throwable as t:
            executionResults.addError(utils.get_message("Unable to read & parse the supplied file parameters.xml, reason: ", t))
    else:
        executionResults.addError("Could not find the mandatory file parameters.xml")

    # We have problem with the inputs, it is time to stop
    if executionResults.getStatus() not in (org.xmcda.ProgramExecutionResult.Status.OK,
                                            org.xmcda.ProgramExecutionResult.Status.WARNING):
        utils.write_program_execution_results(prg_exec_results, executionResults, utils.XMCDA_VERSION_v2)
        return executionResults.getStatus().exitStatus()

    # Convert inputs & parameters to XMCDA v3
    try:
        xmcda_v3 = org.xmcda.converters.v2_v3.XMCDAConverter.convertTo_v3(xmcda_v2)
    except Throwable as t:
        executionResults.addError(utils.get_message("Could not convert inputs (alternatives, criteria, performance table and/or criteria weights) to XMCDA v3, reason: ", t))

    if parameters_v2 is not None:
        try:
            parameters_v3 = org.xmcda.converters.v2_v3.XMCDAConverter.convertTo_v3(parameters_v2)
        except Throwable as t:
            executionResults.addError(utils.get_message("Could not convert parameters to v3, reason: ", t))

    # We have problem with the conversion, it is time to stop
    if executionResults.getStatus() not in (org.xmcda.ProgramExecutionResult.Status.OK,
                                  org.xmcda.ProgramExecutionResult.Status.WARNING):
        utils.write_program_execution_results(prg_exec_results, executionResults, utils.XMCDA_VERSION_v2)
        return executionResults.getStatus().exitStatus()

    # Let's check the inputs
    return_dictionary = {}
    handle_inputs.handle_inputs(xmcda_v3, parameters_v3, executionResults, return_dictionary)
    operator = return_dictionary.get("operator")

    if executionResults.getStatus().name() not in ("OK", "WARNING") or operator is None:
        utils.write_program_execution_results(prg_exec_results, executionResults, utils.XMCDA_VERSION_v2)
        return

    # Here we know that everything was loaded as expected

    # Now let's call the calculation method
    try:
        result_v3 = weightedSum.calculate_weighted_sum(xmcda_v3, operator)
    except Throwable as t:
        executionResults.addError(utils.get_message("The calculation could not be performed, reason: ", t))
        utils.write_program_execution_results(prg_exec_results, executionResults, utils.XMCDA_VERSION_v2)
        return

    # Convert back to v2, and write the results and the messages.xml
    alternatives_values_file = os.path.join(output_dir, "alternativesValues.xml")
    try:
        results_v2 = org.xmcda.converters.v2_v3.XMCDAConverter.convertTo_v2(result_v3)
    except Throwable as t:
        executionResults.addError(utils.get_message("Could not convert alternativesValues into XMCDA_v2, reason: ", t))
        utils.write_program_execution_results(prg_exec_results, executionResults, utils.XMCDA_VERSION_v2)
        return

    try:
        org.xmcda.parsers.xml.xmcda_v2.XMCDAParser.writeXMCDA(results_v2, alternatives_values_file)
    except Throwable as t:
        executionResults.addError(utils.get_message("Error while writing alternativesValues.xml, reason: ", t))
        # Whatever the error is, clean up the file: we do not want to leave an empty or partially-written file
        alternatives_values_file.delete()
        utils.write_program_execution_results(prg_exec_results, executionResults, utils.XMCDA_VERSION_v2)
        return

    # Let's write the file 'messages.xml' as well
    utils.write_program_execution_results(prg_exec_results, executionResults, utils.XMCDA_VERSION_v2)
    return 0


if __name__ == "__main__":
    sys.exit(main())
