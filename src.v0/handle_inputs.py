from java.lang import Throwable

import weightedSum
from utils import get_message
from weightedSum import WEIGHTED_SUM, NORMALIZED_WEIGHTED_SUM


def handle_inputs(xmcda, parameters, executionResults, return_dict):
    # Get the performance table object
    if len(xmcda.performanceTablesList) == 0:
        executionResults.addError("No performance table has been supplied")
    elif len(xmcda.performanceTablesList) > 1:
        executionResults.addError("More than one performance table has been supplied")
    else:
        p = xmcda.performanceTablesList.get(0)

        # Anyhow, it must be numeric here
        if not p.isNumeric():
            executionResults.addError("The performance table must contain numeric values only")
        else:
            # convert all values as Double
            try:
                perf_table = p.asDouble()
                xmcda.performanceTablesList.set(0, perf_table)
            except Throwable as t:
                # Should not happen because we already checked that 'p.isNumeric()' is true
                # However, bugs are always possible... so let's not ignore the throwable
                executionResults.addError(get_message("Error when converting the performance table's value to Double, reason:", t))

    # Let's check the parameters
    with_weights = False

    if xmcda.criteriaValuesList.size() >= 1:
        with_weights = True
    if xmcda.criteriaValuesList.size() > 1:
        executionResults.addError("More than one criteriaValues supplied")

    operator = None

    # weights must be numeric values
    if with_weights:
        weights = xmcda.criteriaValuesList.get(0)
        if not weights.isNumeric():
            executionResults.addError("The weights must be numeric values only")

    examine_parameters = True
    if len(parameters.programParametersList) > 1:
            executionResults.addError("Only one programParameters is expected")
            examine_parameters = False

    parameter_operator_is_present = False
    if len(parameters.programParametersList) > 0 and examine_parameters:
        for prg_param in parameters.programParametersList.get(0):
            if not "operator" == prg_param.id():
                # TODO warning unknown parameter supplied
                continue
            parameter_operator_is_present = True
            if prg_param.getValues() is None or (prg_param.getValues() is not None and len(prg_param.getValues()) > 1):
                executionResults.addError("Parameter operator must have a single (label) value only")
                continue
            operator = prg_param.getValues().get(0).getValue()
            if operator not in weightedSum.AGGREGATION_OPERATORS:
                executionResults.addError('Invalid value for parameter operator, it must be a label, ' +
                                'possible values are: ' + ','.join(weightedSum.AGGREGATION_OPERATORS))
                operator = None

    if operator is not None:
        return_dict['operator'] = operator
    elif not parameter_operator_is_present:
        executionResults.addError("Mandatory parameter 'operator' is missing")

    # weights should be provided for (normalized)weightedSum
    if not with_weights and operator in (WEIGHTED_SUM, NORMALIZED_WEIGHTED_SUM):
        executionResults.addError("Criteria weights must be supplied when parameter operator is " + operator)
