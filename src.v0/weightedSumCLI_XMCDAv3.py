"""
"""

import getopt
import os
import sys

import org.xmcda.ProgramExecutionResult
import org.xmcda.XMCDA
from java.lang import Throwable

import utils
import handle_inputs
import weightedSum


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    input_dir = output_dir = None
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "i:o:h",
                                       ['in=', 'out=', 'help'])
            for opt, arg in opts:
                if opt in ("-h", "--help"):
                    print __doc__
                    sys.exit(0)
                elif opt in ('-i', '--in'):
                    input_dir = arg
                elif opt in ('-o', '--out'):
                    output_dir = arg
        except getopt.error, msg:
            raise Usage(msg)
    except Usage, err:
        print >> sys.stderr, err.msg
        print >> sys.stderr, "for help use --help"
        return 2

    if None in (input_dir, output_dir):
        print >> sys.stderr, "Error: both parameters input and output must be supplied"
        print >> sys.stderr, "for help use --help"
        return 2

    prg_exec_results = os.path.join(output_dir, "messages.xml")

    x_execution_results = org.xmcda.ProgramExecutionResult()

    from org.xmcda.parsers.xml.xmcda_v3 import XMCDAParser
    parser = XMCDAParser()
    # this object is where XMCDA items will be put into.
    xmcda = org.xmcda.XMCDA()

    performance_table_filename = os.path.join(input_dir, "performanceTable.xml")
    if not os.path.exists(performance_table_filename):
        x_execution_results.addError("Could not find the mandatory file performanceTable.xml")
    else:
        try:
            parser.readXMCDA(xmcda, performance_table_filename)
        except Throwable as exc:
            x_execution_results.addError(utils.get_message("Unable to read & parse the performance table, reason: ", exc))

    # TODO check performance table exists and dimensions' sizes != 0
    # TODO forbid creation of alternative+criteria from this point

    # Read the alternatives.xml, if any, because if it is present, it may declare some alternatives to be inactive
    alternatives_file = os.path.join(input_dir, "alternatives.xml")
    if os.path.exists(alternatives_file):
        # TODO check that None are created, and that it is not a subset of performanceTable
        try:
            parser.readXMCDA(xmcda, alternatives_file)
        except Throwable as t:
            x_execution_results.addError(utils.get_message("Unable to read & parse the supplied file for alternatives, reason: ", t))

    # Read the criteria.xml, if any
    criteria_file = os.path.join(input_dir, "criteria.xml")
    if os.path.exists(criteria_file):
        try:
            parser.readXMCDA(xmcda, criteria_file)
        except Throwable as t:
            x_execution_results.addError(utils.get_message("Unable to read & parse the supplied file for criteria, reason: ", t))

    # do we have criteria weights? */
    criteria_weights = os.path.join(input_dir, "criteriaWeights.xml")
    if os.path.exists(criteria_weights):
        try:
            parser.readXMCDA(xmcda, criteria_weights)
        except Throwable as t:
            x_execution_results.addError(
                utils.get_message("Unable to read & parse the supplied file for criteria weights, reason: ", t))

    # Read parameters.xml, if any
    parameters_file = os.path.join(input_dir, "parameters.xml")
    parameters = org.xmcda.XMCDA()
    if os.path.exists(parameters_file):
        try:
            parser.readXMCDA(parameters, parameters_file)
        except Throwable as t:
            x_execution_results.addError(utils.get_message("Unable to read & parse the supplied file parameters.xml, reason: ", t))
    else:
        x_execution_results.addError("Could not find the mandatory file parameters.xml")

    # When we have problem with the inputs, it is time to stop
    if not x_execution_results.isOk() and not x_execution_results.isWarning():
        utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v3)
        return x_execution_results.getStatus().exitStatus()

    # Let's check the inputs
    return_dictionary = {}
    handle_inputs.handle_inputs(xmcda, parameters, x_execution_results, return_dictionary)
    operator = return_dictionary.get("operator")

    if not x_execution_results.isOk() or operator is None:
        utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v3)
        return

    # Here we know that everything was loaded as expected

    # Now let's call the calculation method
    try:
        result = weightedSum.calculate_weighted_sum(xmcda, operator)
    except Throwable as t:
        x_execution_results.addError(utils.get_message("The calculation could not be performed, reason: ", t))
        utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v3)
        return

    alternatives_values_filename = os.path.join(output_dir, "alternativesValues.xml")
    try:
        parser.writeXMCDA(result, alternatives_values_filename)
    except Throwable as t:
        x_execution_results.addError(utils.get_message("Error while writing alternativesValues.xml, reason: ", t))
        # Whatever the error is, clean up the file: we do not want to leave an empty or partially-written file */
        alternatives_values_filename.delete()
        utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v3)
        return

    # Let's write the file 'messages.xml' as well
    utils.write_program_execution_results(prg_exec_results, x_execution_results, utils.XMCDA_VERSION_v3)
    return 0


if __name__ == "__main__":
    sys.exit(main())
